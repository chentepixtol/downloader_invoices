package downloader

class Downloader(val client: InvoiceClient) {

  def countInvoices(id: String, range: DateRange) : DownloaderResult = {
    val result = client.countInvoices(id, range.startDayIsoFormat, range.endDayIsoFormat)
    result match {
      case MoreThan100 => DownloaderResult(range, result, range.split.map(countInvoices(id, _)))
      case _ => DownloaderResult(range, result)
    }
  }

}
