package downloader

import scala.util.Try
import com.typesafe.config.ConfigFactory

object Console extends App {
  val config = ConfigFactory.load

  if (args.length == 2) {
    for {
      id <- Try(args(0))
      year <- Try(args(1).toInt)
    } {
      val range = DateRange(year, 1, DateRange.lastDay(year))
      val client = new HttpInvoiceClient(config.getString("external.invoice.endpoint"))
      val downloader = new Downloader(client)
      val result = downloader.countInvoices(args(0), range)
      println(result.inspect())
    }
  } else {
    usage()
  }

  sys.exit(0)

  def usage() = {
    println("usage sbt \"run ID YEAR\"")
    println("example sbt \"run 4e25ce61-e6e2-457a-89f7-116404990967 2017\"")
  }
}
