package downloader

import dispatch._
import Defaults._

class HttpInvoiceClient(val endpoint: String) extends InvoiceClient {

  def countInvoices(id: String, start: String, finish: String): InvoiceResult = {
    val svc = url(s"$endpoint?id=$id&start=$start&finish=$finish")
    Http.default(svc OK as.String).either() match {
      case Right(response) => response match {
        case "\"Hay más de 100 resultados\"" => MoreThan100
        case "\"Te faltan parámetros\"" => MissingParameters
        case text => InvoiceCount(text.toInt)
      }
      case Left(_) => MissingParameters
    }
  }

}
