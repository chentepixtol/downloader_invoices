package downloader

case class DownloaderResult(dateRange: DateRange, invoiceResult: InvoiceResult, children: List[DownloaderResult] = Nil) {
  def total: Int = {
    invoiceResult match {
      case MoreThan100 => children.map(_.total).sum
      case InvoiceCount(count) => count
      case _ => 0
    }
  }

  def number_of_requests: Int = {
    invoiceResult match {
      case MoreThan100 => 1 + children.map(_.number_of_requests).sum
      case _ => 1
    }
  }

  def inspect(level: Int = 0): String = {
    val invoices = invoiceResult match {
      case MoreThan100 => "More than 100 invoices."
      case InvoiceCount(count) => s"$count invoices."
      case MissingParameters => "Error missing parameters"
    }
    val identation = "  " * level
    val line = s"${identation}From: ${dateRange.startDayIsoFormat} to: ${dateRange.endDayIsoFormat} get ${invoices}\n"
    var summary = ""
    if (level == 0) {
      summary += s"Total of requests: $number_of_requests.\n"
      summary += s"Total of invoices: $total."
    }
    line + children.map(_.inspect(level + 1)).mkString + summary
  }
}