package downloader

import java.util.Calendar
import java.text.SimpleDateFormat

case class DateRange(year: Integer, startDay: Integer, endDay: Integer) {
  require(year >= 1970, "Invalid year argument")
  require(startDay > 0 && startDay <= DateRange.lastDay(year), "Invalid Start day argument")
  require(endDay > 0 && endDay <= DateRange.lastDay(year), "Invalid End day argument")
  require(startDay <= endDay , "Invalid date range")

  lazy val startDayIsoFormat = DateRange.dateToIsoFormat(year, startDay)
  lazy val endDayIsoFormat = DateRange.dateToIsoFormat(year, endDay)

  def sameDay = startDay == endDay

  def split = {
    if (sameDay) {
      throw DateRangeException("Cannot split only one day range")
    }

    val difference = ((endDay - startDay)/ 2).floor.toInt
    List(
      DateRange(year, startDay, startDay + difference),
      DateRange(year, startDay + difference + 1 , endDay)
    )
  }
}

object DateRange {
  def dateToIsoFormat(year: Int, dayOfYear: Int): String = {
    val formatter = new SimpleDateFormat("yyyy-MM-dd")
    val calendar = Calendar.getInstance
    calendar.set(Calendar.YEAR, year)
    calendar.set(Calendar.DAY_OF_YEAR, dayOfYear)
    formatter.format(calendar.getTime)
  }

  def lastDay(year: Int) = if (isLeapYear(year)) 366 else 365

  def isLeapYear(year: Int): Boolean = (year % 4 == 0) && (year % 100 != 0) || (year % 400 == 0)
}
