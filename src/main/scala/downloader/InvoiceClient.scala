package downloader

sealed trait InvoiceResult

object MoreThan100 extends InvoiceResult
object MissingParameters extends InvoiceResult
case class InvoiceCount(count: Integer) extends InvoiceResult {
  require(count <= 100, "The invoices should be lower than 100.")
}

trait InvoiceClient {

  def countInvoices(id: String, start: String, finish: String): InvoiceResult

}
