package downloader

import org.scalatest._

class MockInvoiceClientSpec extends FlatSpec {
  val ID = "fake_id"

  it should "count invoices according test fixtures" in {
    val mockInvoiceClient = new MockInvoiceClient(Map(
      ("2017-01-01", "2017-01-01") -> MoreThan100,
      ("2017-01-02", "2017-01-02") -> MissingParameters,
      ("2017-01-03", "2017-01-05") -> InvoiceCount(50),
    ))
    assert(mockInvoiceClient.countInvoices(ID, "2017-01-01", "2017-01-01") == MoreThan100)
    assert(mockInvoiceClient.countInvoices(ID, "2017-01-02", "2017-01-02") == MissingParameters)
    assert(mockInvoiceClient.countInvoices(ID, "2017-01-03", "2017-01-05") == InvoiceCount(50))
  }
}
