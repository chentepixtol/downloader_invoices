package downloader

class MockInvoiceClient(val fixtures: Map[(String, String), InvoiceResult]) extends InvoiceClient {

  def countInvoices(id: String, start: String, finish: String): InvoiceResult = {
    fixtures.getOrElse((start, finish), InvoiceCount(0))
  }
}
