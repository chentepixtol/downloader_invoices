package downloader

import org.scalatest._

class DownloaderSpec extends FlatSpec {
  val ID = "fake_id"
  it should "count with only one query" in {
    val client = new MockInvoiceClient(Map(
      ("2017-01-01", "2017-12-31") -> InvoiceCount(50),
    ))
    val downloader = new Downloader(client)
    val result = downloader.countInvoices(ID, DateRange(2017, 1, 365))
    assert(result.total == 50)
  }

  it should "count invoices with nesting queries" in {
    val client = new MockInvoiceClient(Map(
      ("2017-01-01", "2017-01-30") -> MoreThan100,
      ("2017-01-01", "2017-01-15") -> InvoiceCount(70),
      ("2017-01-16", "2017-01-30") -> InvoiceCount(80),
    ))
    val downloader = new Downloader(client)
    val result = downloader.countInvoices(ID, DateRange(2017, 1, 30))
    assert(result.total == 150)
  }

  it should "print the verbose information" in {
    val client = new MockInvoiceClient(Map(
      ("2017-01-01", "2017-01-30") -> MoreThan100,
      ("2017-01-01", "2017-01-15") -> InvoiceCount(70),
      ("2017-01-16", "2017-01-30") -> MoreThan100,
      ("2017-01-16", "2017-01-23") -> InvoiceCount(80),
      ("2017-01-24", "2017-01-30") -> InvoiceCount(40),
    ))
    val downloader = new Downloader(client)
    val result = downloader.countInvoices(ID, DateRange(2017, 1, 30))
    assert(result.number_of_requests == 5)
    assert(result.total == 190)
    assertResult(result.inspect()) {
      "From: 2017-01-01 to: 2017-01-30 get More than 100 invoices.\n  " +
        "From: 2017-01-01 to: 2017-01-15 get 70 invoices.\n  " +
        "From: 2017-01-16 to: 2017-01-30 get More than 100 invoices.\n    " +
           "From: 2017-01-16 to: 2017-01-23 get 80 invoices.\n    " +
           "From: 2017-01-24 to: 2017-01-30 get 40 invoices.\n" +
      "Total of requests: 5.\n"  +
      "Total of invoices: 190."
    }
  }
}