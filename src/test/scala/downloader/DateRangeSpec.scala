package downloader

import org.scalatest._

class DateRangeSpec extends FlatSpec {
  it should "have a factory" in {
    val dateRange = DateRange(2017, 1, 365)
    assert(dateRange.isInstanceOf[DateRange])
  }

  it should "validate year value" in {
    assertThrows[IllegalArgumentException] {
      val dateRange = DateRange(1000, 1, 1)
    }
  }

  it should "validate startDay value" in {
    assertThrows[IllegalArgumentException] {
      val dateRange = DateRange(2017, 0, 365)
    }
  }

  it should "validate endDay value" in {
    assertThrows[IllegalArgumentException] {
      val dateRange = DateRange(2017, 1, 0)
    }
  }

  it should "validate end date for leap years" in {
    assertThrows[IllegalArgumentException] {
      val dateRange = DateRange(2017, 1, 366)
    }
    assert(DateRange(2016, 1, 366).endDayIsoFormat == "2016-12-31")
  }

  it should "validate startDay and endDay together" in {
    assertThrows[IllegalArgumentException] {
      val dateRange = DateRange(2017, 10, 5)
    }
  }

  it should "set attribute sameDay if is only one day" in {
    val dateRangeSameDay = DateRange(2017, 1, 1)
    assert(dateRangeSameDay.sameDay == true)

    val dateRangeRangeDays = DateRange(2017, 1, 2)
    assert(dateRangeRangeDays.sameDay == false)
  }

  it should "return format date for start day" in {
    assert(DateRange(2017, 1, 1).startDayIsoFormat == "2017-01-01")
    assert(DateRange(2017, 32, 32).startDayIsoFormat == "2017-02-01")
  }

  it should "return format date for end day" in {
    assert(DateRange(2017, 1, 365).endDayIsoFormat == "2017-12-31")
  }

  it should "split range in two" in {
    assertResult(List(DateRange(2017, 1, 1), DateRange(2017, 2, 2))) {
      DateRange(2017, 1, 2).split
    }

    assertResult(List(DateRange(2017, 1, 2), DateRange(2017, 3, 3))) {
      DateRange(2017, 1, 3).split
    }

    assertResult(List(DateRange(2017, 1, 183), DateRange(2017, 184, 365))) {
      DateRange(2017, 1, 365).split
    }

    assertResult(List(DateRange(2017,16,23), DateRange(2017,24,30))) {
      DateRange(2017, 16, 30).split
    }
  }

  it should "throw exception when is splitting range when is only one day" in {
    assertThrows[DateRangeException] {
      DateRange(2017, 1, 1).split
    }
  }

}