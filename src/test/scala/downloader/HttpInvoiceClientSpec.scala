package downloader

import org.scalatest._

class HttpInvoiceClientSpec extends FlatSpec {
  val ENDPOINT = "http://34.209.24.195/facturas"
  val ID = "4e25ce61-e6e2-457a-89f7-116404990967"

  it should "get invoices for first day of year" in {
    val client = new HttpInvoiceClient(ENDPOINT)
    val result = client.countInvoices(ID, "2017-01-01", "2017-01-01")
    assertResult(result) {
      InvoiceCount(1)
    }
  }

  it should "get more than 100 for all year" in {
    val client = new HttpInvoiceClient(ENDPOINT)
    val result = client.countInvoices(ID, "2017-01-01", "2017-12-31")
    assertResult(result) {
      MoreThan100
    }
  }

  it should "get missing paramters error" in {
    val client = new HttpInvoiceClient(ENDPOINT)
    val result = client.countInvoices(ID, "2017-01-01", "")
    assertResult(result) {
      MissingParameters
    }
  }
}