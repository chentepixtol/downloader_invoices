# Invoice downloader service.

This service is created to get the number of invoices of a company in a time range.

You can run with the following command: 

`sbt "run 4e25ce61-e6e2-457a-89f7-116404990967 2017"`

where the first parameter is the id of the company and the second parameter is the fiscal year where you want to search.

This will return information with the description of the requests, defining the dates and the value obtained in each,
The total number of invoices and the total number of requests are also displayed.

The algorithm created is recursive, where you start with a range of the whole year and it is divided in two
in case of obtaining more than 100 results to cover the full range.

An example of the result is the following:

```
[info] From: 2017-01-01 to: 2017-12-31 get More than 100 invoices.
[info]   From: 2017-01-01 to: 2017-07-02 get More than 100 invoices.
[info]     From: 2017-01-01 to: 2017-04-02 get More than 100 invoices.
[info]       From: 2017-01-01 to: 2017-02-15 get More than 100 invoices.
[info]         From: 2017-01-01 to: 2017-01-23 get 82 invoices.
[info]         From: 2017-01-24 to: 2017-02-15 get 73 invoices.
[info]       From: 2017-02-16 to: 2017-04-02 get More than 100 invoices.
[info]         From: 2017-02-16 to: 2017-03-10 get 75 invoices.
[info]         From: 2017-03-11 to: 2017-04-02 get 71 invoices.
[info]     From: 2017-04-03 to: 2017-07-02 get More than 100 invoices.
[info]       From: 2017-04-03 to: 2017-05-18 get More than 100 invoices.
[info]         From: 2017-04-03 to: 2017-04-25 get 63 invoices.
[info]         From: 2017-04-26 to: 2017-05-18 get 62 invoices.
[info]       From: 2017-05-19 to: 2017-07-02 get More than 100 invoices.
[info]         From: 2017-05-19 to: 2017-06-10 get 71 invoices.
[info]         From: 2017-06-11 to: 2017-07-02 get 66 invoices.
[info]   From: 2017-07-03 to: 2017-12-31 get More than 100 invoices.
[info]     From: 2017-07-03 to: 2017-10-01 get More than 100 invoices.
[info]       From: 2017-07-03 to: 2017-08-17 get More than 100 invoices.
[info]         From: 2017-07-03 to: 2017-07-25 get 67 invoices.
[info]         From: 2017-07-26 to: 2017-08-17 get 94 invoices.
[info]       From: 2017-08-18 to: 2017-10-01 get More than 100 invoices.
[info]         From: 2017-08-18 to: 2017-09-09 get 74 invoices.
[info]         From: 2017-09-10 to: 2017-10-01 get 77 invoices.
[info]     From: 2017-10-02 to: 2017-12-31 get More than 100 invoices.
[info]       From: 2017-10-02 to: 2017-11-16 get More than 100 invoices.
[info]         From: 2017-10-02 to: 2017-10-24 get 72 invoices.
[info]         From: 2017-10-25 to: 2017-11-16 get 82 invoices.
[info]       From: 2017-11-17 to: 2017-12-31 get More than 100 invoices.
[info]         From: 2017-11-17 to: 2017-12-09 get 76 invoices.
[info]         From: 2017-12-10 to: 2017-12-31 get 68 invoices.
[info] Total of requests: 31.
[info] Total of invoices: 1173.
```


## Install ##

To run the program it is necessary to have java installed and sbt which is the builder for scala.

https://www.scala-sbt.org/1.0/docs/Setup.html

Once sbt is installed, just run the following command:

`sbt "run 4e25ce61-e6e2-457a-89f7-116404990967 2017"`

## Unit Testing ##

For run the test suite, you can run the command:

`sbt test`

Which will display the results of the tests.

```
[info] DownloaderSpec:
[info] - should count with only one query
[info] - should count invoices with nesting queries
[info] - should print the verbose information
[info] DateRangeSpec:
[info] - should have a factory
[info] - should validate year value
[info] - should validate startDay value
[info] - should validate endDay value
[info] - should validate end date for leap years
[info] - should validate startDay and endDay together
[info] - should set attribute sameDay if is only one day
[info] - should return format date for start day
[info] - should return format date for end day
[info] - should split range in two
[info] - should throw exception when is splitting range when is only one day
[info] MockInvoiceClientSpec:
[info] - should count invoices according test fixtures
[info] HttpInvoiceClientSpec:
[info] - should get invoices for first day of year
[info] - should get more than 100 for all year
[info] - should get missing paramters error
[info] Run completed in 2 seconds, 440 milliseconds.
[info] Total number of tests run: 18
[info] Suites: completed 4, aborted 0
[info] Tests: succeeded 18, failed 0, canceled 0, ignored 0, pending 0
[info] All tests passed.
[success] Total time: 18 s, completed Dec 11, 2018, 6:59:25 PM
```
