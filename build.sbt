name := "downloader"

version := "0.1"

scalaVersion := "2.12.8"

fork in run := true

libraryDependencies += "org.scalatest" % "scalatest_2.12" % "3.0.5" % "test"
libraryDependencies += "org.dispatchhttp" %% "dispatch-core" % "1.0.0"
libraryDependencies += "com.typesafe" % "config" % "1.3.2"
